from pythonblackjack import blackjack

def test_card_class():
    # Test the card displays as it should
    test_card = blackjack.Card('Ace','Spades')
    assert str(test_card) == "Ace of Spades"
    # Test the card value is correct
    assert test_card.value == 11
    # Try both tests for other cards
    test_card = blackjack.Card("4", "Hearts")
    assert str(test_card) == "4 of Hearts"
    assert test_card.value == 4
    test_card = blackjack.Card("Queen", "Diamonds")
    assert str(test_card) == "Queen of Diamonds"
    assert test_card.value == 10
    # Test the lowering of the ace function
    test_card = blackjack.Card('Ace','Clubs')
    test_card.lower_ace()
    assert test_card.value == 1

def test_new_deck_class():
    # Test a new deck with top card being Ace of Hearts
    newdeck = blackjack.Deck()
    topcard = str(newdeck.cards[0])
    assert topcard == "Ace of Hearts"
    # Test a new deck gives 52 cards
    assert len(newdeck.cards) == 52

def test_shuffle_deck():
    # Test the top card is different after shuffling (slight chance it won't be, but that's bad luck)
    newdeck = blackjack.Deck()
    topcard = str(newdeck.cards[0])
    newdeck.shuffle()
    newtopcard = str(newdeck.cards[0])
    assert topcard != newtopcard

def test_deal():
    # Test the deal function takes the card out of the deck
    newdeck = blackjack.Deck()
    numberofcards = len(newdeck.cards)
    topcard = newdeck.deal()
    assert len(newdeck.cards) == numberofcards-1
    # Test the deal function returns the top card
    assert str(topcard) == "Ace of Hearts"
    # Test the new top card is not the one just dealt
    assert newdeck.cards[0] != topcard

def test_hand():
    # Test dealing from the top of a new unshuffled deck puts the right card in the hand
    newdeck = blackjack.Deck()
    hand = blackjack.Hand('Pytest')
    hand.hit(newdeck)
    assert str(hand.cards[0]) == "Ace of Hearts"
    # Test the hand total is correct
    assert hand.total() == 11
    # Test the deck has removed the card in the hand
    assert len(newdeck.cards) == 51
    # Test dealing the next card (2 of hearts) gives the correct total
    hand.hit(newdeck)
    assert hand.total() == 13
    # Test the second dealt card has been removed from the deck
    assert len(newdeck.cards) == 50
    # Test the hand show function produces the correct output
    assert hand.show() == "Pytest has:\nAce of Hearts\n2 of Hearts\nTotal: 13"
    # Test declaring a new hand empties the hand
    hand = blackjack.Hand('Pytest')
    assert len(hand.cards) == 0

def test_ace_logic():
    hand = blackjack.Hand('Pytest')
    hand.cards.append(blackjack.Card('Ace','Spades'))
    hand.cards.append(blackjack.Card('King','Hearts'))
    assert hand.total() == 21
    hand.cards.append(blackjack.Card('Jack','Diamonds'))
    assert hand.total() == 21
    hand = blackjack.Hand('Pytest')
    hand.cards.append(blackjack.Card('Ace','Spades'))
    hand.cards.append(blackjack.Card('9','Clubs'))
    assert hand.total() == 20
    hand.cards.append(blackjack.Card('8','Spades'))
    assert hand.total() == 18
    hand = blackjack.Hand('Pytest')
    hand.cards.append(blackjack.Card('Ace','Spades'))
    hand.cards.append(blackjack.Card('Ace','Clubs'))
    assert hand.total() == 12
    hand.cards.append(blackjack.Card('Ace','Clubs'))
    assert hand.total() == 13
    hand.cards.append(blackjack.Card('King','Clubs'))
    assert hand.total() == 13