from setuptools import setup
import re
import os
import sys

package = "pythonblackjack"
requirements=[]
test_requirements=[]

setup(
    name="pythonblackjack",
    author="James O'Hara",
    packages = ['pythonblackjack'],
    test_suite='tests'
)