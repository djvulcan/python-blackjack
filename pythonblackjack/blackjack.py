import random
import sys
import time

class Card():
    CARDVALUES = {'Ace': 11, 'Jack': 10, 'Queen': 10, 'King': 10, 'Ace(Low)': 1}
    def __init__(self, card_value, suit):
        self.card_value = card_value
        self.suit = suit

    def __str__ (self):
        """ Output the card in human readable """
        return "{0.card_value} of {0.suit}".format(self)

    @property
    def value(self):
        """return the card values using the above lookup to get values of picture cards"""
        return int(self.CARDVALUES.get(self.card_value, self.card_value))
    
    def lower_ace(self):
        """ Lower the Ace from 11 to 1 """
        if self.card_value == 'Ace':
            self.card_value = 'Ace(Low)'



class Deck():
    def __init__(self):
        SUITS=("Hearts","Diamonds","Clubs","Spades")
        CARD_VALUES=("Ace","2","3","4","5","6","7","8","9","10","Jack","Queen","King")
        self.cards=[Card(card_value,suit) for suit in SUITS for card_value in CARD_VALUES]

    def shuffle(self):
        """ shuffle the deck """
        for _ in range(1,4):
            random.shuffle(self.cards)

    def deal(self):
        """ take the top card out of the deck and output it """
        dealt_card = self.cards[0]
        self.cards.remove(self.cards[0])
        return(dealt_card)

class Hand():
    def __init__(self,owner):
        self.cards=[]
        self.owner = owner

    def hit(self,deck):
        """ Take a card from the deck into the hand """
        self.cards.append(deck.deal())

    def total(self):
        """ Output the total value of the hand """
        # Work out if aces should be counted as 1
        total_non_aces = sum(card.value for card in self.cards if card.value != 11)
        count_high_aces = sum(card.value for card in self.cards if card.value == 11)/11
        if total_non_aces > 10 or count_high_aces > 1:
            for card in self.cards:
                if card.value == 11:
                    card.lower_ace()
                    count_high_aces -= 1
                    if count_high_aces <2 and total_non_aces <= 10:
                        break
        total = sum(card.value for card in self.cards)
        return total
    def show(self):
        """ Output human readable contents of the hand """
        showhand="%s has:\n" % (self.owner)
        for card in self.cards:
            showhand += str(card)
            showhand += "\n"
        showhand +="Total: "
        showhand += str(self.total())
        return showhand

class Game():
    def __init__(self):
        #self.player=player
        #self.dealer=dealer
        self.deck=Deck()

    def hit_or_stick(self):
        choice = input("Hit H to hit or S to stick: ")
        if choice == "s":
            return 0
        else:
            return 1

    def play_round(self):
        playerscore = 0
        dealerscore = 0
        self.deck = Deck()
        self.deck.shuffle()
        self.playerhand = Hand("Player")
        self.dealerhand = Hand("Dealer")
        for i in range(2):
            self.playerhand.hit(self.deck)
        self.dealerhand.hit(self.deck)
        print(self.dealerhand.show())
        print(self.playerhand.show())
        hit = self.hit_or_stick()
        while self.playerhand.total() < 21 and hit:
            self.playerhand.hit(self.deck)
            time.sleep(1)
            print(self.playerhand.show())
            if self.playerhand.total() > 21:
                print("Player busts")
                playerscore = -1
                break
            hit = self.hit_or_stick()
        if playerscore >-1:
            playerscore = self.playerhand.total()
        time.sleep(1)
        print("\nDealer to play\n")
        while self.dealerhand.total() < 17:
            self.dealerhand.hit(self.deck)
            time.sleep(1)
            print(self.dealerhand.show())
            if self.dealerhand.total() > 21:
                print ("Dealer busts")
                dealerscore = -1
                break
        if dealerscore > -1:
            dealerscore = self.dealerhand.total()
        print("\n")
        if playerscore > dealerscore:
            print("Player Wins!!")
        elif playerscore < dealerscore:
            print("Dealer Wins!!")
        elif playerscore == dealerscore:
            print("A Tie!")

if __name__ == "__main__":
    game=Game()
    choice = input("Jimbo's BlackJack.  Press Y for a game  ")
    while choice.lower() == "y":
        game.play_round()
        choice = input("Play again?   Y/N: ")
